import { Component, OnInit } from '@angular/core';
import { GranjaService} from '../granja.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private granjas : any;

  constructor(private granjaService : GranjaService, private router: Router) { }

  ngOnInit() {

    this.granjaService.getGranjasAPI()
    .subscribe(data => {
      console.log(data);
      console.log(data.results);
      this.granjas = data.results;

      console.log("log de this.granjas");
      console.log(this.granjas);      
    },
       error => {
         console.log("fallo el call de la API"); 
         console.log(error)
       });
  
      console.log("porque se ejecuta esto?");
      console.log(this.granjas);
  }

  setGranjaId(id:number){
    this.granjaService.setGranjaId(id);
  }

  verGallinas(id: number) {

    console.log('imprimo parametro del .ts : '+id);
    this.router.navigate(['gallinas',id]);
  }

}
