import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { appRouting} from './app.routes';
import { HomeComponent } from './home/home.component';
import { AppComponent } from './app.component';
import {HttpModule} from '@angular/http';
import { GranjaService } from './granja.service';
import { GallinaComponent } from './gallina/gallina.component';
import { HuevoComponent } from './huevo/huevo.component';
import {FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    GallinaComponent,
    HuevoComponent
  ],
  imports: [
    BrowserModule,
    appRouting,
    HttpModule,
    FormsModule
  ],
  providers: [GranjaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
