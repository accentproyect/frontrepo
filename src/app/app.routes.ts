import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { GallinaComponent } from './gallina/gallina.component';
import { HuevoComponent } from './huevo/huevo.component';

const routes: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'gallinas/:id', component: GallinaComponent },
    { path: 'huevos/:id', component: HuevoComponent }, 
    { path: '**', pathMatch:'full', redirectTo: 'home' }
];

export const appRouting = RouterModule.forRoot(routes);

