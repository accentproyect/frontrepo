import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})
export class GranjaService {

  granjas:any;
  gallinas:any;
  huevos:any;

  private granjaId : number;
  private gallinaId : number;

  constructor(private http:Http) {

    console.log("servicio listo");

   }

   setGranjaId(id:number) {
    this.granjaId = id;
    console.log("seteo id global para granja en el service"+id);
   }

   setGallinaId(id:number) {
     this.gallinaId = id;
     console.log("seteo id global para gallina en el service"+id);
   }

   getGranjaId() {
     return this.granjaId;
   }

   getGallinaId() {
     return this.gallinaId;
   }

   getGranjasAPI(){
  
    console.log("llama a getGranjas API");
  
     let header = new Headers({'Content-Type':'application/json'});
     let granjasURL = "http://localhost:8080/granja";
  
      return this.http.get(granjasURL, {headers: header})
         .map(res =>{
           console.log(res.json());
           console.log("entro positivo REST getGranjasAPI")
           this.granjas = res.json();
           console.log(this.granjas);
           
           // this.granjas = res.json().results;
           return res.json();
         }, err => console.log("error: "+err.json()));
     }

   getGranjas(){
      return this.granjas;
   }  

   getGallinasAPI(id:number){
    console.log("llama a getGallinas API");
    console.log("imprimo id que recibe el api"+id);
     let header = new Headers({'Content-Type':'application/json'});
     let gallinasURL = "http://localhost:8080/granja/granjas/"+id;
  
      return this.http.get(gallinasURL, {headers: header})
         .map(res =>{
           console.log("hago un log de res.json");
           console.log(res.json());
           console.log("hago un log de res.json().results");
           console.log(res.json().results);
           console.log("entro positivo REST getGallinasAPI");
           this.gallinas = res.json().results;
           console.log("hago un log de this.gallinas en el service");
           console.log(this.gallinas);
           return res.json();
         }, err => console.log("error: "+err.json()));
    }

    getHuevosAPI(id:number){

      let header = new Headers({'Content-Type':'application/json'});
      let huevosURL = "http://localhost:8080/granja/gallinas/"+id;

      return this.http.get(huevosURL, {headers: header})
      .map(res =>{
        console.log("hago un log de res.json");
        console.log(res.json());
        console.log("hago un log de res.json().results");
        console.log(res.json().results);
        console.log("entro positivo REST getHuevosAPI");
        this.huevos = res.json().results;
        console.log("hago un log de this.huevos en el service");
        console.log(this.huevos);
        return res.json();
      }, err => console.log("error: "+err.json()));
      
    }

    postHuevoAPI(color:any){

      console.log("llama a postHuevo API");
      console.log("el body que llego es:");
      console.log(color);
      
       let header = new Headers({'Content-Type':'application/json'});
       let huevoURL = "http://localhost:8080/granja/huevos/";

       let body = color;

       console.log("el body es:")
       console.log(body);

        return this.http.post(huevoURL, body, {headers: header})
           .map(res =>{
             console.log(res.json());
             console.log("entro positivo REST postHuevoAPI");
             return res.json();
           }, err => console.log("error: "+err.json()));
       }

      degollarAPI(id:number) {

        console.log("la gallina sera degollada en unos instantes");

        let header = new Headers({'Content-Type':'application/json'});
        let killURL = "http://localhost:8080/granja/"+id;

        return this.http.delete(killURL, {headers: header})
        .map(res =>{
          console.log(res.json());
          console.log("entro positivo REST degollarAPI");
          return res.json();
        }, err => console.log("error: "+err.json()));
        
      }

}
