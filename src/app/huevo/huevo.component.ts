import { Component, OnInit } from '@angular/core';
import { GranjaService } from '../granja.service';
import { Router} from '@angular/router';
import { NgForm }   from '@angular/forms';

@Component({
  selector: 'app-huevo',
  templateUrl: './huevo.component.html',
  styleUrls: ['./huevo.component.css']
})
export class HuevoComponent implements OnInit {

  huevos:any;

  crearHuevo={
    idGallina: null,
    huevo: 
      {color: ''}
  }

  constructor(private granjaService : GranjaService, private router:Router) { }

  ngOnInit() {
    this.granjaService.getHuevosAPI(this.granjaService.getGallinaId())
    .subscribe(data => {
      console.log("log de data");
      console.log(data);
      console.log("log de data.results");
      console.log(data.results);
      this.huevos = data.huevos;
      console.log("log de data.huevos");
      console.log(data.huevos);
      console.log("hago un log de this.huevos");
      console.log(this.huevos);
      
    },
       error => {
         console.log("fallo el call de la API");
       
         console.log(error)
       });
  
      console.log(this.huevos);
  
    }

    guardar(forma: NgForm) {

      this.crearHuevo.idGallina = this.granjaService.getGallinaId();
  
      console.log("log de this.crearHuevo");
      console.log(this.crearHuevo);
  
      console.log("gallinaid seteado: "+this.granjaService.getGallinaId());
  
      this.granjaService.postHuevoAPI(this.crearHuevo)
      .subscribe(data => {
        console.log(data);
        console.log(data.results);
        
        // this.comentarios = data.results;
      },
         error => {
           console.log("fallo el call de la API");
         
           console.log(error)
         });
      
  }

}
