import { Component, OnInit } from '@angular/core';
import { GranjaService } from '../granja.service';
import { Router} from '@angular/router';

@Component({
  selector: 'app-gallina',
  templateUrl: './gallina.component.html',
  styleUrls: ['./gallina.component.css']
})
export class GallinaComponent implements OnInit {

  gallinas:any;

  constructor(private granjaService : GranjaService, private router:Router) { }

  ngOnInit() {
    this.granjaService.getGallinasAPI(this.granjaService.getGranjaId())
    .subscribe(data => {
      console.log("log de data");
      console.log(data);
      console.log("log de data.results");
      console.log(data.results);
      this.gallinas = data.gallinas;
      console.log("log de data.gallinas");
      console.log(data.gallinas);
      console.log("hago un log de this.gallinas");
      console.log(this.gallinas);
      
    },
       error => {
         console.log("fallo el call de la API");
       
         console.log(error)
       });
  
      console.log(this.gallinas);
  
    }

    setGallinaId(id:number) {
      this.granjaService.setGallinaId(id);
    }

    verHuevos(id:number){

      console.log('imprimo el parametro del ts : '+id);
      this.router.navigate(['huevos',id]);
      
    }

    degollar(id:number) {

      console.log('imprimo el id que llego a degollar(): '+id)
      this.granjaService.degollarAPI(id).subscribe(data => {
 
      },
         error => {
           console.log("fallo el call de la API");
         
           console.log(error)
         });

    }

}
