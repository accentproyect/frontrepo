import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GallinaComponent } from './gallina.component';

describe('GallinaComponent', () => {
  let component: GallinaComponent;
  let fixture: ComponentFixture<GallinaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GallinaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GallinaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
